#include <algorithm>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <tclap/CmdLine.h>
#include <vector>

struct atom{
  int serial;                     /*!< serial (progressive) number */
  std::string id;                 /*!< atom identifier (CA, CB, ...) */
  char altLoc;                    /*!< alternative location */
  std::string resName;            /*!< residue name */
  char chainId;                   /*!< chain identifier */
  int resSeq;                     /*!< residue number */
  char iCode;                     /*!< code for insertion of residues */
  Eigen::Vector3d coordinate;     /*!< coordinate */
  double occupancy;               /*!< occupancy */
  double tempFactor;              /*!< temperature factor */
  std::string element;            /*!< element type (carbon, oxygen, ...) */
  double charge;                  /*!< charge */
};

std::ostream& operator << ( std::ostream& out, atom& atm ){
  std::set<std::string> aa_list = {"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL","HIE"};
  if( aa_list.find(atm.resName)!=aa_list.end() ){
    out << "ATOM  ";
  }
  else{
    out << "HETATM";
  }
  out << std::setw(5) << atm.serial;
  out << " ";
  out << std::setw(4) << atm.id;
  out << atm.altLoc;
  out << std::setw(3) << atm.resName;
  out << " ";
  out << atm.chainId;
  out << std::setw(4) << atm.resSeq;
  out << atm.iCode;
  out << "   ";
  out << std::fixed << std::setprecision(3);
  out << std::setw(8) << atm.coordinate(0);
  out << std::setw(8) << atm.coordinate(1);
  out << std::setw(8) << atm.coordinate(2);
  out << std::fixed << std::setprecision(2);
  out << std::setw(6) << atm.occupancy;
  out << std::setw(6) << atm.tempFactor;
  out << "          "; // blank
  out << atm.element;
  if( atm.charge == 0. )
    out << "  ";
  else{
    out << std::setprecision(0) << fabs(atm.charge);
    if( atm.charge > 0 )
      out << '+';
    else if(atm.charge < 0){
      out << '-';
    }
  }
  out << std::endl;
  return out;
};

atom parseAtom(std::string& line){
  atom info;
  auto line_id = line.substr(0,6);
  if(line_id == std::string("ATOM  ") or line_id == std::string("HETATM") ){
    info.serial = atoi(line.substr(6, 5).c_str()); 
    info.id = line.substr(12, 4);
    info.altLoc = line.substr(16, 1).c_str()[0];
    info.resName = line.substr(17, 3);
    info.chainId = isdigit( line.substr(21, 1).c_str()[0] ) ? char( 'A'+atoi(line.substr(21, 1).c_str()) ) : line.substr(21, 1).c_str()[0];
    if(info.chainId == ' ') info.chainId = 'A';
    info.chainId = toupper(info.chainId);
    info.resSeq = atoi( line.substr(22, 4).c_str() );
    info.iCode = line.substr(26, 1).c_str()[0];
    info.coordinate = Eigen::Vector3d( atof(line.substr(30, 8).c_str()), 
                                       atof(line.substr(38, 8).c_str()),
                                       atof(line.substr(46, 8).c_str()));
  
    try{
      info.occupancy = atof(line.substr(54, 6).c_str()) ? atof(line.substr(54, 6).c_str()) : 0.;
    }
    catch(std::out_of_range& oor){
      info.occupancy = 0.0;
    }
  
    try{
      info.tempFactor = atof(line.substr(60, 6).c_str()) ? atof(line.substr(60, 6).c_str()) : 0.;
    }
    catch (std::out_of_range& oor){
      info.tempFactor = 0.0;
    }
    try{
      info.element = line.substr(76,2);
    }
    catch (std::out_of_range& oor){
      info.element = " H";
    }
    info.charge = 0;
    try {
      if( isdigit( line.substr(78, 2).c_str()[0] ) and ( line.substr(78, 2).c_str()[1]=='-' or line.substr(78, 2).c_str()[1]=='+') ){
        info.charge = line.substr(78, 2).c_str()[1]=='-' ? -atof(line.substr(78, 1).c_str()) : atof(line.substr(78, 1).c_str());
      }
    }
    catch (std::out_of_range& oor){
      info.charge = 0;
    }
  }
  return info;
};

// Kabsch algorithm
Eigen::Affine3d Find3DAffineTransform(Eigen::Matrix3Xd in, Eigen::Matrix3Xd out) {

  // Default output
  Eigen::Affine3d A;
  A.linear() = Eigen::Matrix3d::Identity(3, 3);
  A.translation() = Eigen::Vector3d::Zero();

  if (in.cols() != out.cols())
    throw "Find3DAffineTransform(): input data mis-match";

  // First find the scale, by finding the ratio of sums of some distances,
  // then bring the datasets to the same scale.
  double dist_in = 0, dist_out = 0;
  for (int col = 0; col < in.cols()-1; col++) {
    dist_in  += (in.col(col+1) - in.col(col)).norm();
    dist_out += (out.col(col+1) - out.col(col)).norm();
  }
  if (dist_in <= 0 || dist_out <= 0)
    return A;
  double scale = dist_out/dist_in;
  out /= scale;

  // Find the centroids then shift to the origin
  Eigen::Vector3d in_ctr = Eigen::Vector3d::Zero();
  Eigen::Vector3d out_ctr = Eigen::Vector3d::Zero();
  for (int col = 0; col < in.cols(); col++) {
    in_ctr  += in.col(col);
    out_ctr += out.col(col);
  }
  in_ctr /= in.cols();
  out_ctr /= out.cols();
  for (int col = 0; col < in.cols(); col++) {
    in.col(col)  -= in_ctr;
    out.col(col) -= out_ctr;
  }

  // SVD
  Eigen::MatrixXd Cov = in * out.transpose();
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(Cov, Eigen::ComputeThinU | Eigen::ComputeThinV);

  // Find the rotation
  double d = (svd.matrixV() * svd.matrixU().transpose()).determinant();
  if (d >= 0)
    d = 1.0;
  else
    d = -1.0;
  Eigen::Matrix3d I = Eigen::Matrix3d::Identity(3, 3);
  I(2, 2) = d;
  Eigen::Matrix3d R = svd.matrixV() * I * svd.matrixU().transpose();

  // The final transform
  A.linear() = scale * R;
  A.translation() = scale*(out_ctr - R*in_ctr);

  return A;
}

int main(int argc, char* argv[]){
  std::string pdb_filename1 = "";
  std::string pdb_filename2 = "";
  std::string out_filename = "";
  std::string parameter_filename = "";
  bool chainbychain = false;
  
  try{
    TCLAP::CmdLine cmd("Align two structures", ' ', "0.1.1");
    TCLAP::ValueArg<std::string> pdb1Arg(          "s","source",       "in: source pdb file",                           true, "source.pdb",     "string" ); cmd.add( pdb1Arg );
    TCLAP::ValueArg<std::string> pdb2Arg(          "t","target",       "in: target pdb file",                           true, "target.pdb",     "string" ); cmd.add( pdb2Arg );
    TCLAP::ValueArg<std::string> outArg(           "o","out",          "output: aligned pdb file",                      true, "aligned.pdb",    "string" ); cmd.add( outArg );
    TCLAP::ValueArg<std::string> kabschArg(        "k","rototrans",    "output: list of roto-translation parameters",   false, "parameters.dat","string" ); cmd.add( kabschArg );
    TCLAP::ValueArg<bool>        chainbychainArg(  "c","chainbychain", "in: align chain by chain",                      false, false,           "boolean"); cmd.add( chainbychainArg );
    cmd.parse( argc, argv );

    pdb_filename1 = pdb1Arg.getValue();
    pdb_filename2 = pdb2Arg.getValue();
    out_filename = outArg.getValue();
    parameter_filename = kabschArg.getValue();
    chainbychain = chainbychainArg.getValue();
  }
  catch (TCLAP::ArgException &e){
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return 1;
  }

  std::string line;
  std::map< char,std::vector<atom> > system_s, system_t;
  std::set< std::pair<char,int> > ca_s, ca_t;
  std::set< char > chainId;
  
  // read pdb1
  std::map<std::pair<char,int>,Eigen::Vector3d> coordinate_s;
  std::ifstream pdb_file1(pdb_filename1);
  while(std::getline(pdb_file1, line)){
    auto line_id = line.substr(0,6);
    if(line_id == std::string("ATOM  ")){
      auto atm = parseAtom(line);
      if(system_s.find(atm.chainId)==system_s.end()){
        system_s[atm.chainId]={};
        chainId.insert(atm.chainId);
      }
      system_s[atm.chainId].push_back(atm);
      if(atm.id==" CA " or atm.id=="CA  " or atm.id=="  CA"){
        auto pair=std::make_pair(atm.chainId,atm.resSeq);
        ca_s.insert(pair);
        coordinate_s[pair] = atm.coordinate;
      }
    }
  }
  pdb_file1.close();
  
  // read pdb2
  std::map<std::pair<char,int>,Eigen::Vector3d> coordinate_t;
  std::ifstream pdb_file2(pdb_filename2);
  while(std::getline(pdb_file2, line)){
    auto line_id = line.substr(0,6);
    if(line_id == std::string("ATOM  ")){
      auto atm = parseAtom(line);
      if(system_t.find(atm.chainId)==system_t.end()){
        system_t[atm.chainId]={};
      }
      system_t[atm.chainId].push_back(atm);
      if(atm.id==" CA " or atm.id=="CA  " or atm.id=="  CA"){
        auto pair=std::make_pair(atm.chainId,atm.resSeq);
        ca_t.insert(pair);
        coordinate_t[pair] = atm.coordinate;
      }
    }
  }
  pdb_file2.close();
  
  // get list of common CAs
  uint total_ca=0;
  std::map<char, std::vector<Eigen::Vector3d> > ca_common_s,ca_common_t;
  uint n_ca_s = ca_s.size();
  for(auto ca: ca_s){
    auto k = ca_t.find( ca );
    if( k!=ca_s.end() ){
      total_ca+=1;
      ca_common_s[ca.first].push_back(  coordinate_s[ca]  );
      ca_common_t[ca.first].push_back(  coordinate_t[ca]  );
    }
  }
  
  // align
  std::ofstream out_file;
  out_file.open(out_filename.c_str());
  std::ofstream parameter_file;
  parameter_file.open (parameter_filename.c_str());
  if(chainbychain==false){
    Eigen::Matrix3Xd in(3, total_ca), out(3, total_ca);
    uint idx=0;
    for(auto ch: chainId){
      uint size=ca_common_s[ch].size();
      for(uint aa=0; aa!=size; ++aa){
        in.col(idx) =ca_common_s[ch][aa];
        out.col(idx)=ca_common_t[ch][aa];
        ++idx;
      }
    }
    Eigen::Affine3d kabsch = Find3DAffineTransform(in, out);
    auto translation = kabsch.translation();
    auto quaternion = Eigen::Quaternion<double>(kabsch.rotation());
    quaternion.normalize();
    parameter_file << "GLOB" << " " << "GLOB" << " " << translation.transpose() << " " << quaternion.vec().transpose() << " " << quaternion.w() << std::endl;
    
    // print 
    for(auto ch:chainId ){
      for(auto atm:system_s[ch]){
        atm.coordinate = kabsch*atm.coordinate;
        out_file << atm;
      }
    }
  }
  else{
    for(auto ch: chainId){
      uint size=ca_common_s[ch].size();
      
      Eigen::Matrix3Xd in(3, size), out(3, size);
      for(uint aa=0; aa!=size; ++aa){
        in.col(aa) =ca_common_s[ch][aa];
        out.col(aa)=ca_common_t[ch][aa];
      }
      Eigen::Affine3d kabsch = Find3DAffineTransform(in, out);
      auto translation = kabsch.translation();
      auto quaternion = Eigen::Quaternion<double>(kabsch.rotation());
      quaternion.normalize();
      parameter_file << ch << " " << "GLOB" << " " << translation.transpose() << " " << quaternion.vec().transpose() << " " << quaternion.w() << std::endl;
      
      //print
      for(auto atm:system_s[ch]){
        atm.coordinate = kabsch*atm.coordinate;
        out_file << atm;
      }
    }
  }
  out_file.close();
  parameter_file.close();

  return 0;
}
