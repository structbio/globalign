# globalign vers. 0.1.1 #
Align two protein structures

## How it works ##
localign will:
1. read one source and one target structure in **pdb** format
2. select residues which are common between the two
3. find the best global roto-translation 
4. produce an aligned structure for every cluster

## Usage: ##

`globalign  [-k <string>] [-c <string>] [-m <unsigned int>] [-g <double>] [-l <double>] [-p <double>] [-w <unsigned int>] -o <string> -t <string> -s <string> [--] [--version] [-h]`

Where: 

- -s <string>,  --source <string> (input file) source pdb file. **required**.
- -t <string>,  --target <string> (input file) target pdb file. **required**.
- -o <string>,  --out <string> (output structure basename) aligned pdb filename will be of the form 'cl_XX_basename'. **required**.
- -k <string>,  --rototrans <string> (output file) will contain a list of roto-translation parameters for every window and every cluster. Default: //parameters.dat//.
- -c <string>,  --chainbychain <string> (boolean) Align chain by chain. Default: false.
- --version  Displays version information and exits.
- -h,  --help  Displays usage information and exits.

## Compilation ##
`g++ --std=c++11 --fast-math -msse3 -O3 localign.cpp -o globalign`

**Dependencies**
- tclap: for option parsing